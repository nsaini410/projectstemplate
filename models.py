import os
from peewee import CharField, Model, PostgresqlDatabase, DateField


database = PostgresqlDatabase(None)


class Person(Model):
    first_name = CharField()
    last_name = CharField()
    birthday = DateField()

    class Meta:
        database = database


def initialize_database():
    db = os.environ.get('POSTGRES_DB', 'db')
    user = os.environ.get('POSTGRES_USER', 'postgres')
    password = os.environ.get('POSTGRES_PASSWORD', 'postgres')
    host = os.environ.get('POSTGRES_HOST', '127.0.0.1')
    port = int(os.environ.get('POSTGRES_PORT', 5432))
    database.init(db, user=user, password=password, host=host, port=port)
    with database:
        database.create_tables([Person], safe=True)
